import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


@pytest.fixture(scope="session")
def browser():
    chrome_options = Options()
    chrome_options.binary_location = "C:\\Users\\User\\Documents\\pythonProject\\resourse\\chromedriver.exe"
    chrome_options.binary_location = "C:\\Users\\User\\Documents\\pythonProject\\resourse\\\chrome-win64\\chrome.exe"
    driver = webdriver.Chrome(options=chrome_options)
    chrome_options.add_argument('--disable-gpu')
    yield driver
    driver.quit()