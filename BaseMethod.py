import random
from selenium.common import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains


# Реализует стандартные необходимые методы для работы с webdriver,
# используя selenium, типо найти, кликнуть и т.д.
class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://stroylandiya.ru/"

    # Найти ОДИН элемент по локатору
    def find_element(self, locator: object, time: object = 10) -> object:
        # Найти элемент по локатору
        return WebDriverWait(self.driver, time).until(EC.presence_of_element_located(locator),
                                                      message=f"Can't find element by locator {locator}")

    # Найти ВСЕ элементы по локатору
    def find_elements(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.presence_of_all_elements_located(locator),
                                                      message=f"Can't find elements by locator {locator}")

    # проверяет, что элемент присутствует на странице
    def visibility_of_element(self, locator, time=10):
        # Найти Видимый элемент по локатору
        element = WebDriverWait(self.driver, time).until(
            EC.visibility_of_element_located(locator),
            message=f"Element with locator {locator} is not visible"
        )
        return element

    # проверяет, что элемент присутствует на странице и доступен для нажатия (клика)
    def click_element(self, locator, time=10):
        # Найти элемент по локатору и кликнуть
        element = WebDriverWait(self.driver, time).until(
            EC.element_to_be_clickable(locator),
            message=f"Element with locator {locator} is not clickable"
        )
        element.click()

    # Перейти на сайт
    def go_to_site(self):
        # Перейти на сайт
        return self.driver.get(self.base_url)

    # Максимизировать окно
    def maximize_window(self):
        # Максимизировать окно
        return self.driver.maximize_window()

    # Навести курсор на элемент
    def move_to_element(self, element):
        actions = ActionChains(self.driver)
        actions.move_to_element(element).perform()

    # Создать список для хранения отображаемых элементов
    def get_visible_elements(self, elements):
        visible_elements = []
        for element in elements:
            if element.is_displayed():
                visible_elements.append(element.text)
        return visible_elements

    # Проверить видимость элемента
    def is_displayed(self, locator, timeout=10):
        try:
            element = WebDriverWait(self.driver, timeout).until(EC.visibility_of_element_located(locator))
            return element.is_displayed()
        except TimeoutException:
            return False

        # Выбирает случайным образом один элемент из списка elements.
        # Возвращает выбранный элемент.

    def select_random_element(self, elements):
        random_element = random.choice(elements)
        return random_element

    # это ожидание, которое проверяет, что элемент с заданным локатором присутствует в DOM-дереве страницы.
