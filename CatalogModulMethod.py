from selenium.common import TimeoutException, StaleElementReferenceException
from selenium.webdriver import Keys
from selenium.webdriver.support.wait import WebDriverWait
from BaseMethod import BasePage
from Locators import StroyCatalogLocators, PopUpStroyLocators, PageLocators
import time
from selenium.webdriver.support import expected_conditions as EC


# Реализует методы для работы с элементами на веб-страницах.
class CatalogHelper(BasePage):
    # Закрытие popup ВЫБОРА ГОРОДА
    def find_and_close_popup_city(self):
        try:
            close_button = self.find_element(PopUpStroyLocators.LOCATOR_CITY_POPUP)
            close_button.click()

        except TimeoutException as e:
            print("Не удалось найти кнопку закрытия попапа ВЫБОРА ГОРОДА:", str(e))

    def execute_script(self, param, scroll_element):
        pass

    def burger_to_catalog_button_transform(self):
        time.sleep(3)
        # Вычисление стиля трансформ кнопки "=" первая закрытая полоска
        button_x1 = self.find_element(StroyCatalogLocators.LOCATOR_BUTTON_CATALOG_X1)
        COMPUTED_STYLE_PROPERTY1 = button_x1.get_attribute("transform")
        # Вычисление стиля трансформ кнопки "=" третья закрытая полоска
        button_x3 = self.find_element(StroyCatalogLocators.LOCATOR_BUTTON_CATALOG_X3)
        COMPUTED_STYLE_PROPERTY3 = button_x3.value_of_css_property("transform")
        # Клик по кнопке "Каталог" для открытия меню
        self.click_element(StroyCatalogLocators.LOCATOR_BUTTON_CATALOG)
        time.sleep(3)
        # Вычисление стиля трансформ кнопки "Х" первая открытая полоска
        button_x1 = self.find_element(StroyCatalogLocators.LOCATOR_BUTTON_CATALOG_X1)
        COMPUTED_STYLE_PROPERTY2 = button_x1.value_of_css_property("transform")
        # Вычисление стиля трансформ кнопки "Х" вторая открытая полоска
        button_x3 = self.find_element(StroyCatalogLocators.LOCATOR_BUTTON_CATALOG_X3)
        COMPUTED_STYLE_PROPERTY4 = button_x3.value_of_css_property("transform")
        assert COMPUTED_STYLE_PROPERTY1 != COMPUTED_STYLE_PROPERTY2 and COMPUTED_STYLE_PROPERTY3 != COMPUTED_STYLE_PROPERTY4

    def click_button_catalog(self):
        # Клик по кнопке "Каталог" для открытия меню
        self.click_element(StroyCatalogLocators.LOCATOR_BUTTON_CATALOG)

    def visibility_of_element_catalog_scrolable_2_level(self):
        # Найти элемент с указанным локатором
        element = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(StroyCatalogLocators.LOCATOR_MENU_PARENT_LINK_SELECTED),
            message=f"Элемент с локатором {StroyCatalogLocators.LOCATOR_MENU_PARENT_LINK_SELECTED} не видим"
        )
        return element

    def visibility_of_element_button_catalog(self):
        try:
            # Найти элемент с указанным локатором
            element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located(StroyCatalogLocators.LOCATOR_BUTTON_CATALOG),
                message=f"Элемент с локатором {StroyCatalogLocators.LOCATOR_BUTTON_CATALOG} не видим"
            )
            print("Элемент видим")
        except TimeoutException:
            assert False, f"Элемент с локатором {StroyCatalogLocators.LOCATOR_BUTTON_CATALOG} не видим"

    def is_element_not_visible_menu_parent_link_selected(self):
        try:
            # Найти элемент с указанным локатором
            WebDriverWait(self.driver, 10).until(
                EC.invisibility_of_element_located(StroyCatalogLocators.LOCATOR_MENU_PARENT_LINK_SELECTED),
                message=f"Элемент с локатором {StroyCatalogLocators.LOCATOR_MENU_PARENT_LINK_SELECTED} не видим"
            )
        except TimeoutException:
            assert False, f"Элемент с локатором {StroyCatalogLocators.LOCATOR_MENU_PARENT_LINK_SELECTED} видим"

    def is_element_not_visiblestroy_window_catalog_all_menu(self):
        try:
            # Найти элемент с указанным локатором
            WebDriverWait(self.driver, 10).until(
                EC.invisibility_of_element_located(StroyCatalogLocators.WINDOW_CATALOG_ALL_MENU),
                message=f"Элемент с локатором {StroyCatalogLocators.WINDOW_CATALOG_ALL_MENU} не видим"
            )
        except TimeoutException:
            assert False, f"Элемент с локатором {StroyCatalogLocators.WINDOW_CATALOG_ALL_MENU} видим"

    def visibility_of_element_left_part_catalog(self):
        try:
            # Найти элемент с указанным локатором
            element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located(StroyCatalogLocators.LOCATOR_IN_LEFT_PART_CATALOG),
                message=f"Элемент с локатором {StroyCatalogLocators.LOCATOR_IN_LEFT_PART_CATALOG} не видим"
            )
        except TimeoutException:
            assert False, f"Элемент с локатором {StroyCatalogLocators.LOCATOR_IN_LEFT_PART_CATALOG} не видим"

    def top_category_is_selected(self):
        element_1 = self.visibility_of_element(StroyCatalogLocators.LOCATOR_FIRST_IN_PARENT_LINK)
        element_2 = self.visibility_of_element(StroyCatalogLocators.LOCATOR_MENU_PARENT_LINK_SELECTED)
        assert element_1.text == "Новогодние украшения" and element_1 == element_2

    def check_catalog_parent_category_vertical_scroll(self):
        # Наведение курсора на элемент
        element = self.find_element(StroyCatalogLocators.LOCATOR_MENU_PARENT_LINK_SELECTED)
        self.move_to_element(element)

        # Найти все элементы по селектору
        elements = self.find_elements(StroyCatalogLocators.LOCATOR_MENU_PARENT_LINK_SELECTED)

        # Получить видимые элементы
        visible_elements = self.get_visible_elements(elements)

        # Проскролить каталог вниз
        scroll_element = self.find_element(StroyCatalogLocators.LOCATOR_MENU_PARENT_LINK)
        scroll_element.send_keys(Keys.END)  # прокрутка вниз до конца
        time.sleep(2)

        # Наведение курсора на элемент
        element = self.find_element(StroyCatalogLocators.LOCATOR_MENU_PARENT_LINK_SELECTED)
        self.move_to_element(element)

        # Найти все элементы по селектору
        elements_scrolled = self.find_elements(StroyCatalogLocators.LOCATOR_MENU_PARENT_LINK_SELECTED)

        # Получить видимые элементы после прокрутки
        visible_elements_scrolled = self.get_visible_elements(elements_scrolled)

        assert visible_elements != visible_elements_scrolled

    def catalog_navigation_first_to_second_and_third_level(self):
        assert self.find_element(StroyCatalogLocators.LOCATOR_CATEGORY_LEVEL2)
        assert self.find_element(StroyCatalogLocators.LOCATOR_CATEGORY_LEVEL3_GROUP)

    def click_on_the_first_level_category_leads_to_the_first_level_catalog_page(self):
        # Найти все элементы с атрибутом data-id от 0 до 17
        elements = self.find_elements(StroyCatalogLocators.LOCATOR_FIRST_IN_PARENT_LINK)
        filtered_elements = [element for element in elements if
                             element.get_attribute("data-id") in [str(i) for i in range(18)]]

        # Случайным образом выбрать один элемент из списка
        random_element = self.select_random_element(filtered_elements)
        random_element_text = random_element.text

        try:
            # Выполнить действия с выбранным элементом
            random_element.click()

            selected_element = self.find_element(PageLocators.LOCATOR_SELECTED_CATEGORY)
            selected_element_text = selected_element.text
            assert selected_element_text == random_element_text

        except StaleElementReferenceException:
            # Если элемент стал устаревшим, повторно найдите его
            selected_element = self.find_element(PageLocators.LOCATOR_SELECTED_CATEGORY)
            selected_element_text = selected_element.text
            assert selected_element_text == random_element_text

    def click_on_the_second_level_category_leads_to_the_second_level_catalog_page(self):
        # Найти все элементы с атрибутом data-id от 0 до 17
        elements = self.find_elements(StroyCatalogLocators.LOCATOR_FIRST_IN_PARENT_LINK)
        filtered_elements = [element for element in elements if
                             element.get_attribute("data-id") in [str(i) for i in range(18)]]

        # Случайным образом выбрать один элемент из списка
        random_element = self.select_random_element(filtered_elements)
        self.move_to_element(random_element)
        print(random_element.text)
        time.sleep(2)

        elements2 = self.find_elements(StroyCatalogLocators.LOCATOR_CATEGORY_LEVEL2)
        # Создать список видимых элементов
        visible_elements2 = [element for element in elements2 if element.is_displayed()]

        try:
            if visible_elements2:
                # Выбрать первый видимый элемент из списка
                visible_element2 = visible_elements2[0]

                # Кликнуть на видимый элемент
                visible_element2.click()
                print(visible_element2.text)
            else:
                print("No visible elements found")
            selected_element = self.find_element(PageLocators.LOCATOR_SELECTED_CATEGORY)
            selected_element_text = selected_element.text
            assert selected_element_text == visible_element2.text

        except StaleElementReferenceException:
            # Если элемент стал устаревшим, повторно найдите его
            visible_element2 = self.find_element(PageLocators.LOCATOR_SELECTED_CATEGORY)
            visible_element2_text = visible_element2.text
            assert visible_element2_text == visible_element2.text

    def click_on_the_third_level_category_leads_to_the_third_level_catalog_page(self):
        # Найти все элементы с атрибутом data-id от 0 до 17
        elements = self.find_elements(StroyCatalogLocators.LOCATOR_FIRST_IN_PARENT_LINK)
        filtered_elements = [element for element in elements if
                             element.get_attribute("data-id") in [str(i) for i in range(18)]]

        random_element = self.select_random_element(filtered_elements)
        self.move_to_element(random_element)

        elements3 = self.find_elements(StroyCatalogLocators.LOCATOR_CATEGORY_LEVEL3_GROUP)
        visible_elements3 = [element for element in elements3 if element.is_displayed()]

        while not visible_elements3:
            elements3 = self.find_elements(StroyCatalogLocators.LOCATOR_CATEGORY_LEVEL3_GROUP)
            visible_elements3 = [element for element in elements3 if element.is_displayed()]

        try:
            visible_element3 = visible_elements3[0]
            visible_element3.click()
        except IndexError:
            print("No visible elements found")

            selected_element = self.find_element(PageLocators.LOCATOR_FILTER_CATEGORY_ACTIVE)
            selected_element_text = selected_element.text
            assert selected_element_text == visible_element3.text

    def catalog_have_category_banners(self):

        self.move_to_element(self.find_element(StroyCatalogLocators.LOCATOR_SANTECHNIKA))
        assert self.visibility_of_element(StroyCatalogLocators.LOCATOR_CATEGORY_BANNER_VILAGIO)

    def click_on_the_catalog_category_banners(self):
        datalink = self.find_element(StroyCatalogLocators.LOCATOR_CATEGORY_BANNER_VILAGIO)
        atribute_vallue = datalink.get_attribute("data-link")
        print(atribute_vallue)
        self.click_element(StroyCatalogLocators.LOCATOR_CATEGORY_BANNER_VILAGIO)
        current_url = self.driver.current_url
        assert atribute_vallue in current_url, "current_url не содержит часть имени atribute_vallue"

    def catalog_have_3_columns(self):
        element = self.visibility_of_element(StroyCatalogLocators.WINDOW_CATALOG_3_COLUMN)
        column_count = element.value_of_css_property("column-count")
        assert column_count == "3", "The column count is not equal to 3"

    def check_block_category_2_and_3_levels_vertical_scroll(self):
        # Наведение курсора на элемент
        element = self.find_element(StroyCatalogLocators.LOCATOR_SANTECHNIKA)
        self.move_to_element(element)
        time.sleep(3)
        # Найти все элементы по селектору
        self.find_elements(StroyCatalogLocators.LOCATOR_CATEGORY_LEVEL2)

        elements2 = self.find_elements(StroyCatalogLocators.LOCATOR_CATEGORY_LEVEL2)
        # Создать список видимых элементов
        visible_elements2 = [element for element in elements2 if element.is_displayed()]


        if visible_elements2:
            # Выбрать первый видимый элемент из списка
            visible_element2 = visible_elements2[0]
        else:
            print("No visible elements found")

        self.move_to_element(visible_element2)
        time.sleep(3)

        # Проскролить каталог вниз

        visible_element2.send_keys(Keys.END)  # прокрутка вниз до конца
        time.sleep(2)

        # Найти все элементы по селектору
        self.find_elements(StroyCatalogLocators.LOCATOR_CATEGORY_LEVEL2)

        elements2_2 = self.find_elements(StroyCatalogLocators.LOCATOR_CATEGORY_LEVEL2)
        # Создать список видимых элементов
        visible_elements2 += [element for element in elements2_2 if element.is_displayed()]
        time.sleep(2)

        # Получить видимые элементы после прокрутки
        visible_elements_scrolled = self.get_visible_elements(elements2_2)

        assert visible_element2 != visible_elements_scrolled

