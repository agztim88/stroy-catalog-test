from CatalogModulMethod import CatalogHelper
from conftest import browser


# 1. На странице есть кнопка "Каталог"
def test_of_the_presence_the_catalog_button(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.go_to_site()
    stroy_catalog.maximize_window()
    stroy_catalog.find_and_close_popup_city()
    stroy_catalog.visibility_of_element_button_catalog()


# 2. При клике в кнопку "Каталог" открывается меню с разделами каталога
def test_click_button_catalog(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.click_button_catalog()
    assert stroy_catalog.visibility_of_element_catalog_scrolable_2_level()


# 3.Бургер в кнопке "Каталог" меняется на "Х"
def test_burger_to_catalog_button_transform(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.click_button_catalog()
    stroy_catalog.burger_to_catalog_button_transform()


# 4. Повторный клик в кнопку "Каталог" закрывает меню с разделами каталога
def test_burger_catalog_button_close(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.click_button_catalog()
    stroy_catalog.is_element_not_visible_menu_parent_link_selected()
    stroy_catalog.is_element_not_visiblestroy_window_catalog_all_menu()


# 5. В левой части окна, категории первого уровня каталога
def test_left_part_of_window_catalog(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.click_button_catalog()
    stroy_catalog.visibility_of_element_left_part_catalog()


# 6. При открытии окна, выбрана верхняя категория
def test_top_category_is_selected(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.top_category_is_selected()


# 7. Список категорий первого уровня скроллится по вертикали, если все категории не помещаются в окне
# 8. Выбор категорий первого уровня происходит наведением фокуса на пункт списка выполнено в 7 тесте
# добавил ассёрт доп
def test_check_catalog_parent_category_vertical_scroll(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.check_catalog_parent_category_vertical_scroll()


# 9. При выборе категории первого уровня, в центральной части окна,
# появляются категории 2-го и 3-го уровня(если он присутствует)
def test_catalog_navigation_first_to_second_and_third_level(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.catalog_navigation_first_to_second_and_third_level()


# 10 Клик в категорию первого уровня, ведет на страницу каталога первого уровня
def test_click_on_the_first_level_category_leads_to_the_first_level_catalog_page(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.click_on_the_first_level_category_leads_to_the_first_level_catalog_page()


# 11 Клик в категорию второго уровня, ведет на страницу каталога второго уровня
def test_click_on_the_second_level_category_leads_to_the_second_level_catalog_page(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.click_button_catalog()
    stroy_catalog.click_on_the_second_level_category_leads_to_the_second_level_catalog_page()


# 12 Клик в категорию третьего уровня, ведет на страницу листинга товаров,
# в правой части над фильтрами отмечена выбранная категория
def test_click_on_the_third_level_category_leads_to_the_third_level_catalog_page(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.click_button_catalog()
    stroy_catalog.click_on_the_third_level_category_leads_to_the_third_level_catalog_page()


# 13 В правой части окна есть(опционально) баннер с акцией (нет такого функционала,
# есть только баннеры с категориями!!!)
def test_catalog_have_category_banners(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.click_button_catalog()
    stroy_catalog.catalog_have_category_banners()


# 14 Клик в баннер акций ведет на страницу акции (на страницу категории)
def test_click_on_the_catalog_category_banners(browser):
    stroy_catalog = CatalogHelper(browser)

    # stroy_catalog.go_to_site()
    # stroy_catalog.maximize_window()
    # stroy_catalog.find_and_close_popup_gorod()
    #
    # stroy_catalog.click_button_catalog()
    # stroy_catalog.catalog_have_category_banners()

    stroy_catalog.click_on_the_catalog_category_banners()


# 15 В блоке с категориями 2-го и 3-го уровня, максимум 3 столбца
def test_catalog_have_3_columns(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.click_button_catalog()
    stroy_catalog.catalog_have_3_columns()


# 16 Блок с категориями 2-го и 3-го уровня, скроллится по вертикали, если все категории не помещаются на один экран
def test_check_block_category_2_and_3_levels_vertical_scroll(browser):
    stroy_catalog = CatalogHelper(browser)
    stroy_catalog.check_block_category_2_and_3_levels_vertical_scroll()
