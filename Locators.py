from selenium.webdriver.common.by import By


class PopUpStroyLocators:
    LOCATOR_CITY_POPUP = (By.XPATH, "//a[@class='popup__close-x']")


class StroyCatalogLocators:
    LOCATOR_BUTTON_CATALOG = (By.CLASS_NAME, "header-v2-catalog-button")
    WINDOW_CATALOG_ALL_MENU = (By.XPATH, "//div[contains(@class, 'fb-header-catalog-menu_opened')]")
    LOCATOR_BUTTON_CATALOG_X3 = (By.XPATH, "//span[@class='third']")
    LOCATOR_BUTTON_CATALOG_X1 = (By.XPATH, "//span[@class='first']")
    LOCATOR_MENU_PARENT_LINK = (By.XPATH, "//a[@class='fb-header-catalog-menu__parent-link']")
    LOCATOR_MENU_PARENT_LINK_SELECTED = (By.XPATH, "//a[@class='fb-header-catalog-menu__parent-link "
                                                   "fb-header-catalog-menu__parent-menu_selected']")
    LOCATOR_FIRST_IN_PARENT_LINK = (By.XPATH, "//a[@data-id]")
    LOCATOR_SANTECHNIKA = (By.XPATH, "//a[@data-id='1']")
    LOCATOR_CATEGORY_LEVEL2 = (By.XPATH, "//a[@class='fb-catalog-header-group-link__header']")
    LOCATOR_CATEGORY_LEVEL3_GROUP = (By.XPATH, "//a[@class='fb-catalog-header-group-link__link']")
    LOCATOR_IN_LEFT_PART_CATALOG = (By.XPATH, "//div[@class='simplebar-content-wrapper']//a["
                                              "@class='fb-header-catalog-menu__parent-link']")
    # LOCATOR_CATEGORY_BANNER_VILAGIO = (By.XPATH,
    #                                    "//div[@class='fb-header-catalog-menu-children__banner']//span[@data-link]//img[@src='/upload/iblock/d68/gulzi3473jkwi87ix14b4tdb1pvya90q.jpg']")
    WINDOW_CATALOG_3_COLUMN = (By.XPATH, "//div[@class='fb-header-catalog-menu-children__content-menu']")
    LOCATOR_CATEGORY_2AND3_LINK = (By.XPATH, "//div[@class='fb-catalog-header-group-link__content']")
    LOCATOR_CATEGORY_BANNER_VILAGIO = (By.XPATH, "/html/body/div[7]/div[2]/div[2]/div[2]/div/div/div[2]/div/span")


class PageLocators:
    LOCATOR_HEADER_LOGO = (By.XPATH, "//a[@class='header-v2-logo']")
    LOCATOR_SELECTED_CATEGORY = (By.XPATH, "//h1[contains(@class,'fb-font-gilroy')]")
    LOCATOR_FILTER_CATEGORY_ACTIVE = (
        By.XPATH, "//a[contains(@class,'fb-filter-category__list-item fb-filter-category__list-item--active')]")
